within ;
package ROSBall
  package Models
    model EMG49

      Modelica.Electrical.Analog.Basic.Resistor resistor(R=R)
        annotation (Placement(transformation(extent={{-50,10},{-30,30}})));
      Modelica.Electrical.Analog.Basic.Inductor inductor(L=L)
        annotation (Placement(transformation(extent={{-20,10},{0,30}})));
      Modelica.Electrical.Analog.Basic.EMF emf(k=k)
        annotation (Placement(transformation(extent={{0,-10},{20,10}})));
      Modelica.Electrical.Analog.Basic.Ground ground
        annotation (Placement(transformation(extent={{-40,-40},{-20,-20}})));
      Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage annotation (
          Placement(transformation(
            extent={{10,-10},{-10,10}},
            rotation=90,
            origin={-60,0})));
      Modelica.Mechanics.Rotational.Components.IdealGear idealGear(ratio=ratio)
        annotation (Placement(transformation(extent={{40,-10},{60,10}})));
      Modelica.Blocks.Interfaces.RealInput v1
        "Voltage between pin p and n (= p.v - n.v) as input signal"
        annotation (Placement(transformation(extent={{-120,-20},{-80,20}})));
      Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_b1
        "Flange of right shaft"
        annotation (Placement(transformation(extent={{90,-10},{110,10}})));
      Modelica.Mechanics.Rotational.Sensors.AngleSensor angleSensor annotation (
          Placement(transformation(
            extent={{10,-10},{-10,10}},
            rotation=90,
            origin={80,-20})));
      Modelica.Blocks.Math.Gain encoder(k=e)
                                          annotation (Placement(transformation(
            extent={{10,-10},{-10,10}},
            rotation=0,
            origin={60,-40})));
      Modelica.Blocks.Interfaces.RealOutput y1 "Output signal connector"
        annotation (Placement(transformation(
            extent={{10,-10},{-10,10}},
            rotation=90,
            origin={0,-100})));
      parameter Modelica.SIunits.Resistance R "Resistance at temperature T_ref";
      parameter Modelica.SIunits.Inductance L "Inductance";
      parameter Modelica.SIunits.ElectricalTorqueConstant k
        "Transformation coefficient";
      parameter Real ratio "Transmission ratio (flange_a.phi/flange_b.phi)";
      parameter Real e "Encoder gain";
      Modelica.Blocks.Continuous.Der der1
        annotation (Placement(transformation(extent={{40,-50},{20,-30}})));
    equation
      connect(signalVoltage.p, resistor.p) annotation (Line(
          points={{-60,10},{-60,20},{-50,20}},
          color={0,0,255},
          smooth=Smooth.None));
      connect(resistor.n, inductor.p) annotation (Line(
          points={{-30,20},{-20,20}},
          color={0,0,255},
          smooth=Smooth.None));
      connect(inductor.n, emf.p) annotation (Line(
          points={{0,20},{10,20},{10,10}},
          color={0,0,255},
          smooth=Smooth.None));
      connect(emf.n, ground.p) annotation (Line(
          points={{10,-10},{10,-20},{-30,-20}},
          color={0,0,255},
          smooth=Smooth.None));
      connect(signalVoltage.n, ground.p) annotation (Line(
          points={{-60,-10},{-60,-20},{-30,-20}},
          color={0,0,255},
          smooth=Smooth.None));
      connect(emf.flange, idealGear.flange_a) annotation (Line(
          points={{20,0},{40,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(signalVoltage.v, v1) annotation (Line(
          points={{-67,0},{-100,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(angleSensor.phi, encoder.u) annotation (Line(
          points={{80,-31},{80,-40},{72,-40}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(idealGear.flange_b, flange_b1) annotation (Line(
          points={{60,0},{100,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(angleSensor.flange, flange_b1) annotation (Line(
          points={{80,-10},{80,0},{100,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(der1.u, encoder.y) annotation (Line(
          points={{42,-40},{49,-40}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(der1.y, y1) annotation (Line(
          points={{19,-40},{0,-40},{0,-100}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (                                 Diagram(coordinateSystem(
              preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
    end EMG49;

    model EncoderTest
      Modelica.Mechanics.Rotational.Sources.Speed speed
        annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
      Modelica.Blocks.Sources.Constant ticks_per_timebase(k=80)
        annotation (Placement(transformation(extent={{-110,10},{-90,30}})));
      Modelica.Blocks.Math.Gain enc_angle(k=980/(2*Modelica.Constants.pi))
        annotation (Placement(transformation(extent={{60,-10},{80,10}})));
      Modelica.Mechanics.Rotational.Sensors.AngleSensor angleSensor
        annotation (Placement(transformation(extent={{30,-10},{50,10}})));
      Modelica.Mechanics.Rotational.Sensors.AccSensor accSensor
        annotation (Placement(transformation(extent={{30,-70},{50,-50}})));
      Modelica.Blocks.Math.Gain enc_acc(k=980/(2*Modelica.Constants.pi))
        annotation (Placement(transformation(extent={{60,-70},{80,-50}})));
      Modelica.Blocks.Continuous.Der der1
        annotation (Placement(transformation(extent={{90,-10},{110,10}})));
      Modelica.Blocks.Math.Gain to_rad_per_second(k=2*Modelica.Constants.pi)
        annotation (Placement(transformation(extent={{-70,10},{-50,30}})));
      Modelica.Blocks.Sources.Constant encoder_gain(k=980)
        annotation (Placement(transformation(extent={{-110,-30},{-90,-10}})));
      Modelica.Blocks.Math.Division division
        annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
      Modelica.Blocks.Math.Gain timebase(k=0.5)
        annotation (Placement(transformation(extent={{-70,-30},{-50,-10}})));
      Modelica.Blocks.Continuous.Integrator integrator
        annotation (Placement(transformation(extent={{90,-70},{110,-50}})));
    equation
      connect(speed.flange, angleSensor.flange) annotation (Line(
          points={{10,0},{30,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(speed.flange, accSensor.flange) annotation (Line(
          points={{10,0},{20,0},{20,-60},{30,-60}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(angleSensor.phi, enc_angle.u) annotation (Line(
          points={{51,0},{58,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(accSensor.a, enc_acc.u) annotation (Line(
          points={{51,-60},{58,-60}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(enc_angle.y, der1.u) annotation (Line(
          points={{81,0},{88,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(ticks_per_timebase.y, to_rad_per_second.u) annotation (Line(
          points={{-89,20},{-72,20}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(division.y, speed.w_ref) annotation (Line(
          points={{-19,0},{-12,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(encoder_gain.y, timebase.u) annotation (Line(
          points={{-89,-20},{-72,-20}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(timebase.y, division.u2) annotation (Line(
          points={{-49,-20},{-46,-20},{-46,-6},{-42,-6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(to_rad_per_second.y, division.u1) annotation (Line(
          points={{-49,20},{-46,20},{-46,6},{-42,6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(enc_acc.y, integrator.u) annotation (Line(
          points={{81,-60},{88,-60}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{
                -120,-100},{120,100}}), graphics), Icon(coordinateSystem(extent
              ={{-120,-100},{120,100}})));
    end EncoderTest;
  end Models;

  package Records
    record ROSBall

      extends Modelica.Icons.Record;

      parameter Modelica.SIunits.Resistance R = 3 "DC resistance";
      parameter Modelica.SIunits.Inductance L = 0.0015 "Winding inductance";
      parameter Real k = 0.015254 "Torque constant";
      parameter Real ratio = 49 "Gear ratio";
      parameter Real e = 980/(2*Modelica.Constants.pi) "Encoder constant";
      parameter Modelica.SIunits.Inertia J = 1.4063e-5 "Wheel inertia";
      parameter Modelica.SIunits.Length radius = 0.0125 "Wheel radius";
      parameter Modelica.SIunits.Mass m = 30 "ROSBall mass";
    end ROSBall;
  end Records;

  package Experiments

    model VerificationRatedValues
      Models.EMG49 motor(
        R=data.R,
        L=data.L,
        k=data.k,
        ratio=data.ratio,
        e=data.e)
        annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
      Modelica.Blocks.Sources.Step step(height=24)
        annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
      Modelica.Mechanics.Rotational.Components.Inertia inertia(J=data.J)
        annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
      Records.ROSBall data(k=0.0325)
        annotation (Placement(transformation(extent={{-90,70},{-70,90}})));
      Modelica.Mechanics.Rotational.Sources.LinearSpeedDependentTorque load(
          tau_nominal=-1.57, w_nominal=12.775810124598)
        annotation (Placement(transformation(extent={{40,-10},{20,10}})));
    equation
      connect(step.y, motor.v1) annotation (Line(
          points={{-49,0},{-40,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(motor.flange_b1, inertia.flange_a) annotation (Line(
          points={{-20,0},{-10,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(load.flange, inertia.flange_b) annotation (Line(
          points={{20,0},{10,0}},
          color={0,0,0},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{
                -100,-100},{100,100}}), graphics),
        experiment(StopTime=0.01, __Dymola_NumberOfIntervals=5000),
        __Dymola_Commands(file="plot1.mos" "plot1"));
    end VerificationRatedValues;

    model VerificationTranslational
      Models.EMG49 motor(
        R=data.R,
        L=data.L,
        k=data.k,
        ratio=data.ratio,
        e=data.e)
        annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
      Modelica.Blocks.Sources.Step step(height=24)
        annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
      Modelica.Mechanics.Rotational.Components.Inertia inertia(J=data.J)
        annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
      Records.ROSBall data(k=0.0325)
        annotation (Placement(transformation(extent={{-90,70},{-70,90}})));
      Modelica.Mechanics.Rotational.Components.IdealRollingWheel wheel(radius=
            data.radius)
        annotation (Placement(transformation(extent={{20,-10},{40,10}})));
      Modelica.Mechanics.Translational.Sensors.SpeedSensor speed annotation (
          Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=90,
            origin={50,20})));
      Modelica.Mechanics.Translational.Components.Mass mass(m=data.m)
        annotation (Placement(transformation(extent={{60,-10},{80,10}})));
    equation
      connect(step.y, motor.v1) annotation (Line(
          points={{-49,0},{-40,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(motor.flange_b1, inertia.flange_a) annotation (Line(
          points={{-20,0},{-10,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(wheel.flangeR, inertia.flange_b) annotation (Line(
          points={{20,0},{10,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(wheel.flangeT, speed.flange) annotation (Line(
          points={{40,0},{50,0},{50,10}},
          color={0,127,0},
          smooth=Smooth.None));
      connect(mass.flange_a, wheel.flangeT) annotation (Line(
          points={{60,0},{40,0}},
          color={0,127,0},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{
                -100,-100},{100,100}}), graphics), experiment(StopTime=3,
            __Dymola_NumberOfIntervals=5000),
        __Dymola_Commands(file="plot2.mos" "plot2"));
    end VerificationTranslational;

    model VerificationControlPID
      Models.EMG49 motor(
        R=data.R,
        L=data.L,
        k=data.k,
        ratio=data.ratio,
        e=data.e)
        annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
      Modelica.Mechanics.Rotational.Components.Inertia inertia(J=data.J)
        annotation (Placement(transformation(extent={{20,-10},{40,10}})));
      Records.ROSBall data(k=0.0325)
        annotation (Placement(transformation(extent={{-90,70},{-70,90}})));
      Modelica.Mechanics.Rotational.Components.IdealRollingWheel wheel(radius=
            data.radius)
        annotation (Placement(transformation(extent={{50,-10},{70,10}})));
      Modelica.Mechanics.Translational.Sensors.SpeedSensor speed annotation (
          Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=90,
            origin={80,20})));
      Modelica.Mechanics.Translational.Components.Mass mass(m=data.m)
        annotation (Placement(transformation(extent={{90,-10},{110,10}})));
      Modelica.Blocks.Sources.Step step(height=980)
        annotation (Placement(transformation(extent={{-90,-10},{-70,10}})));
      Modelica.Blocks.Continuous.LimPID PID(
        Ti=0.2,
        k=0.02,
        yMax=24,
        controllerType=Modelica.Blocks.Types.SimpleController.PID,
        Td=0.1)
        annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
    equation
      connect(motor.flange_b1, inertia.flange_a) annotation (Line(
          points={{10,0},{20,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(wheel.flangeR, inertia.flange_b) annotation (Line(
          points={{50,0},{40,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(wheel.flangeT, speed.flange) annotation (Line(
          points={{70,0},{80,0},{80,10}},
          color={0,127,0},
          smooth=Smooth.None));
      connect(mass.flange_a, wheel.flangeT) annotation (Line(
          points={{90,0},{70,0}},
          color={0,127,0},
          smooth=Smooth.None));
      connect(step.y, PID.u_s) annotation (Line(
          points={{-69,0},{-52,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(PID.y, motor.v1) annotation (Line(
          points={{-29,0},{-10,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(motor.y1, PID.u_m) annotation (Line(
          points={{0,-10},{0,-20},{-40,-20},{-40,-12}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,
                -100},{120,100}}),      graphics), experiment(StopTime=3,
            __Dymola_NumberOfIntervals=5000),
        __Dymola_Commands(file="plot2.mos" "plot2", file="plotControl.mos"
            "plotControl"),
        Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
    end VerificationControlPID;

    model VerificationControlPI
      Models.EMG49 motor(
        R=data.R,
        L=data.L,
        k=data.k,
        ratio=data.ratio,
        e=data.e)
        annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
      Modelica.Mechanics.Rotational.Components.Inertia inertia(J=data.J)
        annotation (Placement(transformation(extent={{20,-10},{40,10}})));
      Records.ROSBall data(k=0.0325)
        annotation (Placement(transformation(extent={{-90,70},{-70,90}})));
      Modelica.Mechanics.Rotational.Components.IdealRollingWheel wheel(radius=
            data.radius)
        annotation (Placement(transformation(extent={{50,-10},{70,10}})));
      Modelica.Mechanics.Translational.Sensors.SpeedSensor speed annotation (
          Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=90,
            origin={80,20})));
      Modelica.Mechanics.Translational.Components.Mass mass(m=data.m)
        annotation (Placement(transformation(extent={{90,-10},{110,10}})));
      Modelica.Blocks.Sources.Step step(height=980)
        annotation (Placement(transformation(extent={{-90,-10},{-70,10}})));
      Modelica.Blocks.Continuous.LimPID PI(
        Ti=0.2,
        k=0.02,
        yMax=24,
        Td=0.1,
        controllerType=Modelica.Blocks.Types.SimpleController.PI)
        annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
    equation
      connect(motor.flange_b1, inertia.flange_a) annotation (Line(
          points={{10,0},{20,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(wheel.flangeR, inertia.flange_b) annotation (Line(
          points={{50,0},{40,0}},
          color={0,0,0},
          smooth=Smooth.None));
      connect(wheel.flangeT, speed.flange) annotation (Line(
          points={{70,0},{80,0},{80,10}},
          color={0,127,0},
          smooth=Smooth.None));
      connect(mass.flange_a, wheel.flangeT) annotation (Line(
          points={{90,0},{70,0}},
          color={0,127,0},
          smooth=Smooth.None));
      connect(step.y, PI.u_s) annotation (Line(
          points={{-69,0},{-52,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(PI.y, motor.v1) annotation (Line(
          points={{-29,0},{-10,0}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(motor.y1, PI.u_m) annotation (Line(
          points={{0,-10},{0,-20},{-40,-20},{-40,-12}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,
                -100},{120,100}}),      graphics), experiment(StopTime=3,
            __Dymola_NumberOfIntervals=5000),
        __Dymola_Commands(file="plot2.mos" "plot2", file="plotControl.mos"
            "plotControl"),
        Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
    end VerificationControlPI;

    package Obsolete
      extends Modelica.Icons.ObsoleteModel;
      model VerificationNoLoad
        Models.EMG49 motor(
          R=data.R,
          L=data.L,
          k=data.k,
          ratio=data.ratio,
          e=data.e)
          annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
        Modelica.Mechanics.Rotational.Components.Inertia inertia(J=data.J)
          annotation (Placement(transformation(extent={{20,-10},{40,10}})));
        Records.ROSBall data(k=0.0325)
          annotation (Placement(transformation(extent={{-90,70},{-70,90}})));
        Modelica.Blocks.Sources.Step step(height=980)
          annotation (Placement(transformation(extent={{-90,-10},{-70,10}})));
        Modelica.Blocks.Continuous.LimPID PI(
          Ti=0.2,
          k=0.02,
          yMax=24,
          Td=0.1,
          controllerType=Modelica.Blocks.Types.SimpleController.PI)
          annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
      equation
        connect(motor.flange_b1, inertia.flange_a) annotation (Line(
            points={{10,0},{20,0}},
            color={0,0,0},
            smooth=Smooth.None));
        connect(step.y, PI.u_s) annotation (Line(
            points={{-69,0},{-52,0}},
            color={0,0,127},
            smooth=Smooth.None));
        connect(PI.y, motor.v1) annotation (Line(
            points={{-29,0},{-10,0}},
            color={0,0,127},
            smooth=Smooth.None));
        connect(motor.y1, PI.u_m) annotation (Line(
            points={{0,-10},{0,-20},{-40,-20},{-40,-12}},
            color={0,0,127},
            smooth=Smooth.None));
        annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                  -100},{100,100}}),      graphics));
      end VerificationNoLoad;

      model BodePlotControlPID
        Models.EMG49 motor(
          R=data.R,
          L=data.L,
          k=data.k,
          ratio=data.ratio,
          e=data.e)
          annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
        Modelica.Mechanics.Rotational.Components.Inertia inertia(J=data.J)
          annotation (Placement(transformation(extent={{20,-10},{40,10}})));
        Records.ROSBall data(k=0.0325)
          annotation (Placement(transformation(extent={{-90,70},{-70,90}})));
        Modelica.Mechanics.Rotational.Components.IdealRollingWheel wheel(radius=
              data.radius)
          annotation (Placement(transformation(extent={{50,-10},{70,10}})));
        Modelica.Mechanics.Translational.Sensors.SpeedSensor speed annotation (
            Placement(transformation(
              extent={{-10,-10},{10,10}},
              rotation=90,
              origin={80,20})));
        Modelica.Mechanics.Translational.Components.Mass mass(m=data.m)
          annotation (Placement(transformation(extent={{90,-10},{110,10}})));
        Modelica.Blocks.Continuous.LimPID PID(
          Ti=0.2,
          k=0.02,
          yMax=24,
          controllerType=Modelica.Blocks.Types.SimpleController.PID,
          Td=0.1)
          annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
        Modelica.Blocks.Interfaces.RealInput u_s1
          "Connector of setpoint input signal"
          annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
        Modelica.Blocks.Interfaces.RealOutput v1
          "Absolute velocity of flange as output signal"
          annotation (Placement(transformation(extent={{110,30},{130,50}})));
      equation
        connect(motor.flange_b1, inertia.flange_a) annotation (Line(
            points={{10,0},{20,0}},
            color={0,0,0},
            smooth=Smooth.None));
        connect(wheel.flangeR, inertia.flange_b) annotation (Line(
            points={{50,0},{40,0}},
            color={0,0,0},
            smooth=Smooth.None));
        connect(wheel.flangeT, speed.flange) annotation (Line(
            points={{70,0},{80,0},{80,10}},
            color={0,127,0},
            smooth=Smooth.None));
        connect(mass.flange_a, wheel.flangeT) annotation (Line(
            points={{90,0},{70,0}},
            color={0,127,0},
            smooth=Smooth.None));
        connect(PID.y, motor.v1) annotation (Line(
            points={{-29,0},{-10,0}},
            color={0,0,127},
            smooth=Smooth.None));
        connect(motor.y1, PID.u_m) annotation (Line(
            points={{0,-10},{0,-20},{-40,-20},{-40,-12}},
            color={0,0,127},
            smooth=Smooth.None));
        connect(PID.u_s, u_s1) annotation (Line(
            points={{-52,0},{-120,0}},
            color={0,0,127},
            smooth=Smooth.None));
        connect(speed.v, v1) annotation (Line(
            points={{80,31},{80,40},{120,40}},
            color={0,0,127},
            smooth=Smooth.None));
        annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,
                  -100},{120,100}}),      graphics), experiment(StopTime=3,
              __Dymola_NumberOfIntervals=5000),
          __Dymola_Commands(file="plot2.mos" "plot2", file="plotControl.mos"
              "plotControl"),
          Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
      end BodePlotControlPID;
    end Obsolete;
  end Experiments;
  annotation (uses(Modelica(version="3.2.1")));
end ROSBall;
