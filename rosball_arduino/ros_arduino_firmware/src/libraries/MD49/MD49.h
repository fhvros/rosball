/* MD49.h

    This file contains the class definition of the MD49-class,
    which is the driver for using a MD49 motor driver connected
    to an Arduino Leonardo via UART on pins 0, 1.

    This file is part of ROSBALL.

    author: Moritz Stüber
    email:  moritz.stueber@students.fhv.at
    date:   2015-12-09
*/

#ifndef MD49_h
#define MD49_h

#include "Arduino.h"

class MD49 {
  public:
    // Constructor
    MD49();
    ~MD49();

    // Methods
    int getSpeedM1();
    int getSpeedM2();
    long getEncoderM1();
    long getEncoderM2();
    int getBatteryVoltage();
    float getCurrentM1();
    float getCurrentM2();

    int getAccRate();
    int getMode();
    int getFirmwareVersion();
    int getError();

    void setSpeedM1(int _speed);
    void setSpeedM2(int _speed);
    void setAccRate(int _acc);
    void setMode(int _mode);
    void resetEncoders();
    void regulatorEnable();
    void regulatorDisable();
    void timeoutEnable();
    void timeoutDisable();

    //     getVI();        // XXX return type?
    //     getEncoders();  // XXX return type?

  private:
    long baud;
};

#endif
