/* MD49.cpp

    This file implements the control of the MD49 motor driver
    via the Serial1-connection of an Arduino Leonardo.

    This file is part of ROSBALL.

    author: Moritz Stüber
    email:  moritz.stueber@students.fhv.at
    date:   2015-12-09
*/


// includes
#include "MD49.h"

// defines
#define CMD         byte(0x00)
#define GET_SPEED1       0x21
#define GET_SPEED2       0x22
#define GET_ENC1         0x23
#define GET_ENC2         0x24
#define GET_ENC          0x25
#define GET_V            0x26
#define GET_I1           0x27
#define GET_I2           0x28
#define GET_VER          0x29
#define GET_ACC          0x2A
#define GET_MODE         0x2B
#define GET_VI           0x2C
#define GET_ERROR        0x2D

#define SET_SPEED1       0x31
#define SET_SPEED2       0x32
#define SET_ACC          0x33
#define SET_MODE         0x34
#define RESET_ENC        0x35
#define REG_DISABLE      0x36
#define REG_ENABLE       0x37
#define TIMEOUT_DISABLE  0x38
#define TIMEOUT_ENABLE   0x39

#define UVLO        B10000000
#define OVLO        B01000000
#define M2SH        B00100000
#define M1SH        B00010000
#define M2TR        B00001000
#define M1TR        B00000100

#define DELAY            50


MD49::MD49() {
  baud = 38400L;
  Serial1.begin(baud);

  // pin assignments?
  // define pin modes?
}

MD49::~MD49() {
  Serial1.end();
}

int MD49::getSpeedM1() {
  byte ans = 0;

  Serial1.write(CMD);
  Serial1.write(GET_SPEED1);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial1.read();
  }
  return int(ans);
}

int MD49::getSpeedM2() {
  byte ans = 0;

  Serial1.write(CMD);
  Serial1.write(GET_SPEED2);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial1.read();
  }
  return int(ans);
}

long MD49::getEncoderM1() {
  long ans;

  Serial1.write(CMD);
  Serial1.write(GET_ENC1);
  delay(DELAY);
  if (Serial1.available() > 3) {
    ans = Serial1.read() << 24ul;
    ans += Serial1.read() << 16ul;
    ans += Serial1.read() << 8ul;
    ans += Serial1.read();
  }
  return ans;
}

long MD49::getEncoderM2() {
  long ans;

  Serial1.write(CMD);
  Serial1.write(GET_ENC2);
  delay(DELAY);
  if (Serial1.available() > 3) {
    ans = Serial1.read() << 24ul;
    ans += Serial1.read() << 16ul;
    ans += Serial1.read() << 8ul;
    ans += Serial1.read();
  }
  return ans;
}

int MD49::getBatteryVoltage() {
  byte ans = 0;

  Serial1.write(CMD);
  Serial1.write(GET_V);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial1.read();
  }
  return int(ans);
}

float MD49::getCurrentM1() {
  byte ans = 0;

  Serial1.write(CMD);
  Serial1.write(GET_I1);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial1.read();
  }
  return float(ans)/10;
}

float MD49::getCurrentM2() {
  byte ans = 0;

  Serial1.write(CMD);
  Serial1.write(GET_I2);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial1.read();
  }
  return float(ans)/10;
}

int MD49::getFirmwareVersion() {
  byte ans = 0;

  Serial1.write(CMD);
  Serial1.write(GET_VER);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial1.read();
  }
  return int(ans);
}

int MD49::getAccRate() {
  byte ans = 0;

  Serial1.write(CMD);
  Serial1.write(GET_ACC);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial1.read();
  }
  return int(ans);
}

int MD49::getMode() {
  byte ans = 0;
  
  Serial1.write(CMD);
  Serial1.write(GET_MODE);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial1.read();
  }
  return int(ans);
}

int MD49::getError() {
  byte ans = 0;

  Serial1.write(CMD);
  Serial1.write(GET_ERROR);
  delay(DELAY);
  if (Serial1.available() > 0) {
    ans = Serial.read();
  }
  if ((ans & UVLO) == UVLO) {
    return 7;  // voltage below 16V
  } else if ((ans & OVLO) == OVLO) {
    return 6;  // voltage above 30V
  } else if ((ans & M2SH) == M2SH) {
    return 5;  // motor 2 short-circuited
  } else if ((ans & M1SH) == M1SH) {
    return 4;  // motor 1 short-circuited
  } else if ((ans & M2TR) == M2TR) {
    return 3;  // current drawn by motor 2 > 10A
  } else if ((ans & M1TR) == M1TR) {
    return 2;  // current drawn by motor 1 > 10A
  } else return 0;
}

void MD49::setSpeedM1(int _speed) {
  Serial1.write(CMD);
  Serial1.write(SET_SPEED1);
  Serial1.write(byte(_speed));
}

void MD49::setSpeedM2(int _speed) {
  Serial1.write(CMD);
  Serial1.write(SET_SPEED2);
  Serial1.write(byte(_speed));
}

void MD49::setAccRate(int _rate) {
  Serial1.write(CMD);
  Serial1.write(SET_ACC);
  Serial1.write(byte(_rate));
}

void MD49::setMode(int _mode) {
  Serial1.write(CMD);
  Serial1.write(SET_MODE);
  Serial1.write(byte(_mode));
  
  /* Setting speeds to zero depending on mode doesn't work here, take care of
   * that yourself! I don't know why, and I don't have time to investigate it.
   */
}

void MD49::resetEncoders() {
  Serial1.write(CMD);
  Serial1.write(RESET_ENC);
}

void MD49::regulatorEnable() {
  Serial1.write(CMD);
  Serial1.write(REG_ENABLE);
}

void MD49::regulatorDisable() {
  Serial1.write(CMD);
  Serial1.write(REG_DISABLE);
}

void MD49::timeoutEnable() {
  Serial1.write(CMD);
  Serial1.write(TIMEOUT_ENABLE);
}

void MD49::timeoutDisable() {
  Serial1.write(CMD);
  Serial1.write(TIMEOUT_DISABLE);
}

