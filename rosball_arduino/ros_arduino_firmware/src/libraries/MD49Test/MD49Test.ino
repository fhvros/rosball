/* MD49.ino

    This file can be used for testing the MD49 motor driver
    via the Serial1-connection of an Arduino Leonardo.

    This file is part of ROSBALL.

    author: Moritz Stüber
    email:  moritz.stueber@students.fhv.at
    date:   2015-12-09
*/


// includes
#include <MD49.h>

// defines
#define SLOW 50
#define STOP 0

// global variables
MD49 md49 = MD49();
byte command;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
 
  // settings
  md49.regulatorDisable();
  md49.timeoutDisable();
  md49.setMode(1);
  md49.resetEncoders();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {
    command = Serial.read();
    switch (command) {
      case 49:
        Serial.println("requesting battery voltage, result:");
        Serial.println(md49.getBatteryVoltage(), DEC);
        break;
      case 50:
        Serial.println("requesting firmware version, result:");
        Serial.println(md49.getFirmwareVersion(), DEC);
        break;
      case 51:
        Serial.println("requesting encoder data, result:");
        for (int i = 0; i < 4; i++) {
          Serial.println(md49.getEncoderM1(), DEC);
          Serial.println(md49.getEncoderM2(), DEC);
          Serial.println("__");
          delay(500);
        }
        break;
      case 52:
        Serial.println("resetting encoders");
        md49.resetEncoders();
        break;
      case 53:
        Serial.println("starting motors");
        md49.setSpeedM1(SLOW);
        md49.setSpeedM2(SLOW);
        break;
      case 54:
        Serial.println("stopping motors");
        md49.setSpeedM1(STOP);
        md49.setSpeedM2(STOP);
        break;
      case 55:
        Serial.println("mode, with delay:");
        Serial.println(md49.getMode(), DEC);
        break;
      case 56:
        Serial.println("set mode to 1");
        md49.setMode(1);
        break;
      default:
        Serial.println("didn't recognize that, try again");
        break;
    }
  }
}
