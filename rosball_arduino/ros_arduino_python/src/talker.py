#!/usr/bin/env python

import rospy
from std_msgs.msg import String


def talker():
    pub = rospy.Publisher("collector", String, queue_size=10)
    rospy.init_node("talker", anonymous=True)
    rate = rospy.Rate(2)
    counter = 0

    while not rospy.is_shutdown():
        if counter < 10:
            msg = "run"
            counter += 1
        elif 10 <= counter < 20:
            msg = "stop"
            counter += 1
            if counter == 19:
                counter = 0
        rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
