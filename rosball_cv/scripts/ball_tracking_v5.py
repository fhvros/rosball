from __future__ import print_function
import sys
import rospy
import cv2
import roslib
import actionlib
#from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from sensor_msgs.msg import Image
import sensor_msgs.point_cloud2 as pc2
from cv_bridge import CvBridge, CvBridgeError
from geometry_msgs.msg import Point, PoseWithCovarianceStamped, Quaternion, PointStamped
import tf
from math import radians, degrees
from std_msgs.msg import String
from cv_bridge import CvBridge
import numpy as np
import cv2.cv as cv
import math
from operator import itemgetter

# Load manifest rosball computer vision
roslib.load_manifest('rosball_cv')

global cv_image


class image_converter:


    def __init__(self):
        #self.image_pub = rospy.Publisher("image_topic_2",Image)

        cv2.namedWindow("Image window", 1)
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("/camera/rgb/image_rect_color", Image, self.callback)
        self.depth_sub = rospy.Subscriber("/camera/depth/points", pc2.PointCloud2, self.depth_callback)

        self.mask_old = None

    def callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            self.detection(cv_image)
        except CvBridgeError as e:
            print(e)

    def depth_callback(self, ros_point_cloud):
        self.pcl = ros_point_cloud
        width = ros_point_cloud.width
        height = ros_point_cloud.height

    def detection(self, cv_image):
        # define the lower and upper boundaries of the "green"
        # ball in the HSV color space, then initialize the
        # list of tracked points
        yellowLower = (28, 86, 6)  #29 86 6
        yellowUpper = (64, 255, 255) #64 255 255

        blurred = cv2.GaussianBlur(cv_image, (11, 11), 0) # orginial 11 11 as parameter
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

        # construct a mask for the color "green", then perform
        # a series of dilations and erosions to remove any small
        # blobs left in the mask
        mask = cv2.inRange(hsv, yellowLower, yellowUpper)
        kernel = np.ones((1, 1), np.uint8) #original 1
        # dilate before erode with different iteration sizes
        mask = cv2.dilate(mask, kernel, iterations=5) # 4 works
        mask = cv2.erode(mask, kernel, iterations=2) #2 works, although higher then dilation seems better


        # TESTING
        res = cv2.bitwise_and(cv_image, cv_image, mask=mask)
        rospy.Subscriber("/camera/depth/points", Point)

        # find contours in the mask and initialize the current
        # (x, y) center of the ball
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        center = None

        i = len(cnts)

        cnts.sort(key=cv2.contourArea, reverse=True)

        points = []

        # only proceed if at least one contour was found
        while i != 0:
            # find the largest contour in the mask, then use
            # it to compute the minimum enclosing circle and
            # centroid
            c = max(cnts, key=cv2.contourArea)

            del cnts[0]
            ((x, y), radius) = cv2.minEnclosingCircle(c)

            # INSERT the function call for pointcloud detection here
            # Afterwards publish it as NAV GOAL
            target_point = pc2.read_points(self.pcl, skip_nans=False, uvs=[[int(x), int(y)]])

            int_position = next(target_point)
            X, Y, Z = int_position

            if not math.isnan(X):
                points.append([x, y, radius, Z * radius, X, Y, Z])

            i -= 1

        points = sorted(points, key=itemgetter(3))

        for point in points:
            # only proceed if the radius meets a minimum size
            if point[2] > 15:
                # draw the circle and centroid on the frame,
                # then update the list of tracked points
                cv2.circle(cv_image, (int(point[0]), int(point[1])), int(point[2]), (0, 255, 255), 2)
                cv2.circle(cv_image, center, 5, (0, 0, 255), -1)
                print (float(point[4]), float(point[5]), float(point[6]))

                pub = rospy.Publisher("/rosball/ball_target", PointStamped, queue_size=10)

                # Make a point msg for the transformer
                goal = PointStamped()
                # TODO: Make PointStampedMsg by adding header
                goal.point.x = float(point[4]) * (-1)
                goal.point.y = float(point[5])
                goal.point.z = float(point[6]) + 0.40
                goal.header.stamp = rospy.Time.now()
                goal.header.frame_id = '/camera_link'

                pub.publish(goal)
                break

        cv2.imshow("Image window", cv_image)
        cv2.waitKey(3)

    # def create_nav_goal(self, Z, Y, Yaw):
    #     """Create a MoveBaseGoal with x, y position and yaw rotation (in degrees). Returns a MoveBaseGoal"""
    #     mb_goal = MoveBaseGoal()
    #     mb_goal.target_pose.header.frame_id = '/map'  # Note: the frame_id must be map
    #     mb_goal.target_pose.pose.position.x = Z # difference in orientation of kinect coord. and rosball coord. system
    #     mb_goal.target_pose.pose.position.y = Y
    #     mb_goal.target_pose.pose.position.z = 0.0  # z must be 0.0 (no height in the map)
    #
    #     # Orientation of the robot is expressed in the yaw value of euler angles
    #     angle = radians(Yaw)  # angles are expressed in radians
    #     quat = tf.quaternion_from_euler(0.0, 0.0, angle)  # roll, pitch, yaw
    #     mb_goal.target_pose.pose.orientation = Quaternion(*quat.tolist())
    #
    #     return mb_goal


def main(args):
    rospy.init_node('image_converter', anonymous=False)
    ic = image_converter()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
        cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)