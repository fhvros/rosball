from __future__ import print_function
import sys
import rospy
import cv2
import roslib
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Point, PoseWithCovarianceStamped, Quaternion, PointStamped
from tf.transformations import quaternion_from_euler, euler_from_quaternion
from math import radians, degrees
from std_msgs.msg import String

# Load manifest rosball computer vision
roslib.load_manifest('rosball_cv')

# global variables init for create nav goal
pose = PoseWithCovarianceStamped()
target = PointStamped()


# Creates navigation goal with the current pose of the robot and the target pose
def create_nav_goal():
    rospy.loginfo("Navigation goal created")
    global pose, target
    """Create a MoveBaseGoal with x, y position and yaw rotation (in degrees). Returns a MoveBaseGoal"""
    mb_goal = MoveBaseGoal()
    mb_goal.target_pose.header.frame_id = '/map'  # Note: the frame_id must be map
    mb_goal.target_pose.pose.position.x = target.point.x
    mb_goal.target_pose.pose.position.y = target.point.y
    mb_goal.target_pose.pose.position.z = 0.0  # z must be 0.0 (no height in the map)

    # Orientation of the robot is expressed in the yaw value of euler angles
    # TODO: Check where angle starts and if conversions are correct
    #angle = radians(yaw + Yaw)  # angles are expressed in radians
    quat = quaternion_from_euler(0.0, 0.0, 0.0)  # roll, pitch, yaw
    mb_goal.target_pose.pose.orientation = Quaternion(*quat.tolist())
    rospy.loginfo("Navigation goal created")
    return mb_goal


# # Callback function for the actual pose of the robot
# def callback_pose(data):
#     """Callback for the topic subscriber. Prints the current received data on the topic."""
#     rospy.loginfo("actual pose received")
#     global pose
#     #x = data.pose.pose.position.x
#     #y = data.pose.pose.position.y
#     #roll, pitch, yaw = euler_from_quaternion([data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w])
#     #rospy.loginfo("Current robot pose: x=" + str(x) + "y=" + str(y) + " yaw=" + str(degrees(yaw)) + " grad")

#TODO: Change to new structure with input PointStamped
# Gets the target coordinates from the ball detection program
# creates a nav goal with the offset values
def callback_target(data):
    global target
    target = data

def main(args):
    rospy.init_node('collector_controller', anonymous=True)

    # rospy Subscribers
    rospy.Subscriber("/rosball/goal", PointStamped, callback_target)
#    rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped, callback_pose)
    rospy.loginfo("Subscribing to /amcl_pose...")

    # rospy Publisher
    pub = rospy.Publisher("/collector", String, queue_size=10)
    rospy.logdebug("Publisher for collector motor created")

    # Connect to nav action server
    nav_as = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
    rospy.loginfo("Connecting to /move_base AS...")
    nav_as.wait_for_server()
    rospy.loginfo("Connected.")

    rospy.loginfo("Creating navigation goal...")
    nav_goal = create_nav_goal()
    rospy.loginfo("Sending goal")

    # Publish to collector topic for turning on collector motor
    pub = rospy.Publisher("collector", String, queue_size=10)

    # TODO: Make run depending on the rest distance to the goal
    coll = "run"
    pub.publish(coll)
    rospy.logdebug("Turn on collector motor")

    # TODO Goals should be only send once
    nav_as.send_goal_and_wait(nav_goal)
    rospy.loginfo("Waiting for result...")
    nav_res = nav_as.get_result()
    nav_state = nav_as.get_state()

    rospy.loginfo("Done!")
    print ("Result: ", str(nav_res)) # always empty, be careful
    print ("Nav state: ", str(nav_state))
    coll = "stop"

    rospy.sleep(10.0)
    pub.publish(coll)
    rospy.loginfo("Turn off collector motor")

    if nav_state == 3:
        rospy.loginfo("Grande success")
    elif nav_state == 4:
        rospy.logerr("Mission aborted")
    elif nav_state == 5:
        rospy.logerr("Goal rejected")

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
        cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
