#!/usr/bin/env python
import rospy
import tf
from geometry_msgs.msg import PointStamped, Point
from std_msgs.msg import Header

goal = PointStamped()

# Gets a PointStamped Message as Input
# Output ist the Point in the /map frame
def callback(data):
    global goal
    # Provide new goal globally
    # TODO check whether goal i really new or only buggy new
    goal = data

    return data

if __name__ == '__main__':

    global goal

    rospy.init_node('tf_bridge', log_level=rospy.DEBUG)
    point_goal = rospy.Subscriber("/rosball/ball_target", PointStamped, callback)
    pub = rospy.Publisher("/rosball/goal", PointStamped, queue_size=10)
    listener = tf.TransformListener()
    transformer = tf.TransformerROS()

    rate = rospy.Rate(10.0)
    rospy.sleep(1.0)
    while not rospy.is_shutdown():
        try:
            # Transform Point to map
            goal_map = listener.transformPoint("map", goal)
            pub.publish(goal_map)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

        rate.sleep()