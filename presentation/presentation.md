# Final Presentation

* Task

* ROS -- what (core, libraries, tools), why, what are nodes, publish/subscribe-mechanism, open source, large community [visualisation messaging?]

* System Architecture -- CS, sensors [drawing]
    - Low-Level: Arduino -- MD49-class, interface to ROS via serial [class diagram]
        + Drive: DC motor + gear + encoder
        + MD49: internal controller
        + Arduino: provides interface to ROS node
    - Arduino-node: twist in, odometry out [msg definition]
    - Two Modes: 
        + manual -- teleop sends twist messages
        + automatic -- twist messages calculated from navigation goal on map [screenshot rviz]
* How?
    - Navigation Stack [from wiki] // parameters!!
    - Image Processing [low pass, threshold, segmentation, improve mask, wheighting]

* Challenges

* Simulation

* Reality -- list other bugs as well

* Demonstration
	

# ToDo

* 
