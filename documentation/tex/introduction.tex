\chapter{Introduction}\pagestyle{plain}
\label{chptr:intro}

In robotics research, an especially interesting topic is the connection of independent mobile units which coordinate their actions in order to accomplish a certain goal. In such applications, the software that determines the robots behaviour has to be distributed across different units and each unit has to communicate with others. This makes the development of such software a challenging task, especially when starting from scratch. Fortunately, an open-source framework called ROS\footnote{\url{http://www.ros.org/}} has been developed which naturally runs on distributed nodes. Many libraries are available, providing both hardware interfaces as well as commonly used functionality, such as autonomous navigation.

One possible use-case for the application of autonomous mobile robots is the collection of tennis balls on a tennis court. In this document, the ROS-based control software of a robot used for the collection of tennis balls is explained.

\section{Task Description}\label{sec:task}
As part of the specialisation ``Applied Robotics'' in the mechatronics programme at Vorarlberg University of Applied Sciences (FHV), students have to apply their knowledge in a project. One of the projects carried out in the winter semester 2015/2016, called \rosball, was comprised of the following tasks:

\setstretch{1.0}
\begin{itemize}
\item \rosball{} starts in the aLab (at the control box of the ABB ISRB 4600) and drives to a predefined position in the corridor to the aLab.
\item Upon arrival, \rosball{} looks for tennis balls and collects them
\item \rosball{} then returns to its starting position
\item At all times, \rosball{} avoids obstacles using a camera, a Microsoft Kinect or a laser scanner
\item The behaviour of \rosball{} can be controlled using simple commands given as QR-codes
\item All parts of the project can be simulated using Gazebo\footnote{\url{http://gazebosim.org/}}
\end{itemize}
\setstretch{1.15}

The necessary hardware for achieving these goals was given. Due to the overall complexity, the control of \rosball{} using QR-codes was excluded from the project. Therefore, five main challenges had to be overcome:

\setstretch{1.0}
\begin{itemize}
\item Get familiar with the principles and programming of ROS
\item Get familiar with the hardware and associated drivers
\item Set up the simulation using ROS and Gazebo
\item Program the detection of tennis balls using image processing methods
\item Connect all parts and verify that they work as expected
\end{itemize}
\setstretch{1.15}

In the next section, a brief summary of the project outcomes is given in order to facilitate the understanding of the following chapters.

\section{Project Overview}\label{sec:overview}
\rosball{} is the name of a small mobile robot designed for collecting tennis balls. It is a differential robot driven by two DC motors with integrated encoders. The motors are independently controlled using a commercial motor controller. This controller receives commands from an Arduino Leonardo via a 5V serial connection. In order to integrate the Arduino into ROS, the firmware provides a set of commands to the host via its USB connection. The host, a laptop called \textsc{Rosdevelmobile}, is running a ROS node which receives the desired velocities from the ROS network, sends them to the motor controller, reads back the encoder counts, calculates the odometry data and publishes it. Additionally, a laser scanner and a Microsoft Kinect are connected to \textsc{Rosdevelmobile}, which runs corresponding nodes and publishes the measurement data to the ROS network. In figure \ref{fig:overview}, the different parts of \rosball{} and their interconnections are visualized.
\myfigure{overview}{1}{fig:overview}{System Architecture}{System Architecture}{b}

\rosball{} has two basic modes of navigation: manual and automatic. In manual mode, the robots movements can be controlled using a joystick or keyboard. Measurements from the laser scanner are used to build a map of the environment (``slamming''). In automatic mode, navigation goals can be set by the user and the robot autonomously finds its way to the destination given a map of the environment.

Images and depth information provided by the Kinect are used to identify tennis balls in the vicinity of the robot. In order to collect them, navigation goals are sent to the path planner and the ball collection mechanism is activated. It consists of an uncontrolled DC motor which rotates two cylinders made of foamed plastic. The tennis balls are caught and then moved to a storage box in the back of the robot.

The state of the system can be visualized using a tool called rviz\footnote{\url{http://wiki.ros.org/rviz}} -- a screenshot of \rosball{} in action is shown in figure \ref{fig:rviz}. Typically, rviz is run on a stationary computer called \textsc{Rosdevelmain}, which is part of the ROS network. \textsc{Rosdevelmain} could also be used for performing computationally expensive operations, provided that the network can handle the necessary traffic\footnote{Because bandwidth limitations do become a problem when using camera data and because \textsc{Rosdevelmobile} should not loose its connection to the wireless network during operation, a separate network has been set up. See the appendix (section ``\nameref{appendix:network}'') for details.}.
\myfigure{rviz}{1}{fig:rviz}{Screenshot rviz}{Visualisation of the current state of the system in rviz (autonomous navigation). In this picture, the robot's footprint, the most important coordinate systems and measurement results of the laser scanner are shown on a map of the aLab. The map was created automatically while driving around the lab in manual mode. Black pixels represent solid objects, areas in light gray represent free space. The areas painted in dark gray should never be crossed by the base\_link-coordinate system of the robot in order to avoid collisions. Intersections of the footprint with these areas is possible, however.}{h}