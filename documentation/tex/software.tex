\chapter{Software}\label{chptr:software}

The main focus of this project is the software that enables the robot to collect tennis balls as a ROS node -- more or less autonomously. Most of it was not written from scratch, but integrating the different parts in order to perform as a whole proved to be a challenging task nonetheless.

In this chapter, a brief introduction to ROS is given and the prerequisites for creating ROS-based applications are outlined. Then, the most important parts of \rosball's software are explained. 

\section{ROS Overview}\label{sec:ros_overview}
The Robot Operating System (ROS) is a framework for developing complex robotic applications in distributed environments. Its main goal is to facilitate the development and testing of robotic control software, which may involve different hardware platforms and distributed application scenarios. ROS provides a messaging system using the asynchronous publish/subscribe model, libraries which implement commonly used functionalities as well as tools for visualisation and simulation. The core components of ROS are licensed under a permissive BSD license, which means ROS is free software which can also be used in commercial closed source projects. However, collaboration is strongly encouraged and there is a large community which contributes packages, documentation and answers to specific questions.

A ROS-based application consists of different packages which are configured using YAML-files. Due to the modular design of ROS, it is possible to use exactly those parts of ROS that are needed for a certain project. There is a package management system called catkin which takes care of dependencies and installation of packages. New packages can be written using \CC{} or Python (which was used in this project). Applications are started by executing launch files from the command line. In a launch file, all nodes necessary for a given task as well as their configuration parameters are defined.

ROS is meant to be run on Ubuntu Linux. In the following chapters, familiarity with ROS, Python, Ubuntu Linux (using a command line, network configuration, installation of software, access rights, SSH) and version management using git is assumed. If the reader does not posess the required skills, the authors recommend the following list of literature hints as a starting point for acquiring them.
\clearpage

\setstretch{1.0}
\begin{description}
\item[ROS] Robot Operating System
  \begin{itemize}
  \item \href{http://www.ros.org/}{ROS Website}, sections ``About'' and ``Why ROS?''
  \item \href{http://wiki.ros.org/}{ROS Wiki}
  \item Joseph, Lentin (2015): Learning Robotics using Python. Design, simulate, program and prototype an interactive autonomous mobile robot from scratch with the help of Python, ROS and Open-CV! First edition: PACKT Publishing.
  \end{itemize}
\item[Python] General purpose, high-level programming language
  \begin{itemize}
  \item \href{https://docs.python.org/3/tutorial/index.html}{Python Tutorial}
  \item \href{https://www.jetbrains.com/pycharm/}{PyCharm IDE}, free community edition
  \end{itemize}
\item[Ubuntu] Debian-based Linux Operating System
  \begin{itemize}
  \item \href{https://wiki.ubuntuusers.de/Startseite/}{wiki.ubuntuusers.de} (in german)
  \item \href{http://linuxcommand.org/}{Linuxcommand.org}
  \end{itemize}
\item[git] Distributed version control system
  \begin{itemize}
  \item \href{http://git-scm.com/}{Git Website}
  \item \href{http://git-scm.com/book/en/v2/Getting-Started-About-Version-Control}{Git Getting Started}
  \end{itemize}
\end{description}
\setstretch{1.15}


\section{Software Architecture}\label{sec:software:architecture}
The control software of \rosball{} is spread across four packages and two stacks\footnote{A stack is a set of packages that belong together}. In figure \ref{fig:architecture}, the packages/stacks, their responsibilities and the most important relations between them are shown. Note that although it looks like one, figure \ref{fig:architecture} is \emph{not} a UML-class diagram.
%use image to give overview, explain packages that don't get a separate chapter, summarize others .. bringup, topics, whole system overview -- which stacks used, main parameters, how do they interact?

\myfigure{architecture}{1}{fig:architecture}{Software Architecture}{Graphical Representation of \rosball's Software Architecture. Rectangles with simple borders represent packages, two lines represent stacks.}{h}

\begin{description}
	\item[bringup] The \lstinline|bringup|-package contains task-specific launch files which are called from a command line when \rosball{} is started. Additionally, all maps are stored in this directory.
	%map storage, task-specific launch files -- each package has low-level launch files which are then called by launch files in bringup-package XXX example?
	\item[teleop] The \lstinline|teleop|-package contains two nodes. One receives joystick messages (published on topic \lstinline|\joy|), converts them to twist messages and publishes them on \lstinline|\cmd_vel|. The other one also publishes twist messages, but uses the keyboard as input device instead.
	%receives joystick messages on topic joy, converts joystick inputs to twist messages, publishes twist messages on topic cmd\_vel /reads keyboard, converts input to twist messages, publishes twist messages on topic cmd\_vel
	\item[navigation] The navigation-stack is responsible for path planning in automatic mode. A graphical overview of how the stack works can be seen in figure \ref{fig:navigation}. 
	
	\myfigure{navigation_stack}{0.8}{fig:navigation}{Navigation-Stack}{Overview Navigation-Stack}{h}
	
	The navigation-stack is a core package of ROS, therefore it only had to be configured (robot footprint, maximal acceleration and speed, inflation around obstacles, \dots). See \url{http://wiki.ros.org/navigation/Tutorials/RobotSetup} for a detailed tutorial on the setup of this stack.
	%give rough idea of how stack works, how it is used and what was configured (costmaps, planner)
	\item[bv] The bv-package contains the source code which implements the detection of tennis balls in the vicinity of the robot using the image and the point cloud data the Kinect provides.
	\item[simulation] In this package, the simulation is set up. See chapter \ref{simulation} for a detailed description.
	\item[arduino] This package contains the firmware that runs on the Arduino as well as the Python-files which implement the corresponding ROS-node.% A detailed description follows in the next section.
\end{description}

The source files of these packages/stacks are located in \lstinline|~/catkin_ws/src/rosball/|, which is under version control (git-repository hosted on Bitbucket). Due to the complexity of this project, it is strongly recommended to consequently use git at all times!


\section{Arduino -- Stack}\label{sec:ros_arduino}
A package called ``\href{https://github.com/hbrobotics/ros_arduino_bridge/tree/indigo-devel}{ros\_arduino\_bridge}'' was used for integrating the Arduino (respectively the firmware that runs on it) into ROS. The package is hardware-specific and intended to be used with Pololu or Robogaia Mega shields, but can be adapted to different hardware easily.

The \lstinline|ros_arduino_bridge|-package consists of two parts: the sketch that runs on the Arduino and the Python-files that implement the ROS node. The main task of the Python files is to receive so-called ``Twist'' messages (desired linear and angular velocities for X, Y, Z), send appropriate commands to the Arduino, calculate odometry data from the received encoder counts and publish it to the ROS network. A more detailed (but not entirely complete) list of what the individual files do can be found below.

\input{tex/arduino_stack}

In order to use this stack with \rosball, only minor modifications had to be made. Namely, a method which turns the collection mechanism on/off was added and the possibility to set the mode of the MD49 motor controller was integrated. Updating the PID parameters of the control on the Arduino during set up was commented out (internal controller MD49 used) because it sometimes took up to a few minutes. Additionally, the publishing rates were increased. All needed parameters are configured in \lstinline|rosball/rosball_arduino/ros_arduino_python/config/rosball_params.yaml|.

% originally designed for, class diagram, how our use case differs from theirs, necessary changes to source code, configuration, interface to rest of ROS network, calculations of desired motor speeds/odometry data from encoder counts

\section{Tennisball Detection}\label{sec:software:balldetection}
Because the aim of the project was to detect and collect tennisballs a detection and control had to be implemented. This mainly splits into two parts, the detection node and the controller node.

\subsection{Detection and computer vision}

The ball detection for \rosball{} needs a ros image topic bridged to a image of opencv. Therefore at the beginning the conversion to opencv is done as well as the input of pointcloud of the kinect. For this code to work a kinect is needed. To detect the colour of the tennisball, which is yellow, a segmentation is necessary. The easiest way to do this is in the HSV colourspace, this requires a upper and lower limit for the colour. Now finished with the preparations, the kinect image is blurred with a Gaussian blur and afterwards converted to HSV colourspace. Afterwards a mask is constructed with opencv. Therefore the original image is compared with the range in between the lower and upper limit.
Because there are still blobs and distortions left in the mask, dilations and erodes have to performed. The parameters of this operations were chosen by trial-and-error technique. This all results in the final mask (figure \ref{fig:mask}).

\begin{lstlisting}[language=python]
# define the lower and upper boundaries of the "yellow"
# ball in the HSV color space, then initialize the
# list of tracked points
yellowLower = (28, 86, 6)  #29 86 6
yellowUpper = (64, 255, 255) #64 255 255

blurred = cv2.GaussianBlur(cv_image, (11, 11), 0) 
hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

# construct a mask for the color "yellow", then perform
# a series of dilations and erosions to remove any small
# blobs left in the mask
mask = cv2.inRange(hsv, yellowLower, yellowUpper)
kernel = np.ones((1, 1), np.uint8) 
# dilate before erode with different iteration sizes
mask = cv2.dilate(mask, kernel, iterations=5) 
mask = cv2.erode(mask, kernel, iterations=2) 
\end{lstlisting}

\myfigure{mask}{0.4}{fig:mask}{Mask for Ball Detection}{Final mask after colour detection with range, dilate and erode}{h}

Gotten the mask, now a bitwise shift of the original image with the mask is performed. This has the advantage that only the right colour is represented in the image with a black background for all other colours not matching a tennisball.

\begin{lstlisting}[language = python]
res = cv2.bitwise_and(cv_image, cv_image, mask=mask)
\end{lstlisting}

\myfigure{res}{0.4}{fig:res}{Mask with Colour Segmentation}{Only yellow parts of the image are represented in front of a black background}{h}

Making use of an opencv method the masked image is searched for contours resulting in a list containing the contour area and the position. 
This list is sorted with the biggest contour above all others. With the result for the position of the contour as x,y coordinates, now the pointcloud is used. With the PCL library containing a method for reading points of the cloud, the 3D-position of the ball in the kinect coordinate frame can be obtained. The coordinate system of the kinect is represented in the /tf topic of \rosball. Therefore, a transform between the kinect and the map frame should be existent all the time. 

The idea from this certain point, was to give the position of the ball in the /map frame with a transform between this two frames. 

\subsection{Collector controller}

The collector controller mainly fulfills two tasks. It turns the collector motor on and off, whether a tennisball is in range or not. 
And second it sends navigation goals to the navigation stack. The basic idea is that the point of the detected ball in the /kinect frame is transformed to the 
/map frame. Then the collector controller builds a simple goal and sends it.

\begin{lstlisting}[language = python]
  # Connect to nav action server
    nav_as = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
    rospy.loginfo("Connecting to /move_base AS...")
    nav_as.wait_for_server()
    rospy.loginfo("Connected.")

    rospy.loginfo("Creating navigation goal...")
    nav_goal = create_nav_goal()
    rospy.loginfo("Sending goal to x= " + str(x) + "y= " + str(y) + "yaw " + str(yaw))

    # Publish to collector topic for turning on collector motor
    pub = rospy.Publisher("collector", String, queue_size=10)
    # TODO: Make run depending on the rest distance to the goal
    coll = "run"
    pub.publish(coll)
    rospy.logdebug("Turn on collector motor")

    # TODO Goals should be only send once
    nav_as.send_goal_and_wait(nav_goal)
    rospy.loginfo("Waiting for result...")
    nav_res = nav_as.get_result()
    nav_state = nav_as.get_state()
    rospy.loginfo("Done!")
    print ("Result: ", str(nav_res)) # always empty, be careful
    print ("Nav state: ", str(nav_state))
    coll = "stop"
    pub.publish(coll)
    rospy.loginfo("Turn off collector motor")
\end{lstlisting}





