\chapter{Simulation}\label{simulation}
	
	In this chapter the simulation environment used - Gazebo - will be outlined with the most important features and ideas behind it and subsequently the implementation of ROSBALL in Gazebo will be discussed superficiall as well as the usage of the created simulations will be explained. 

	\section{Gazebo Overview}
	
		As specified by the given task, it is required to simulate the robot in the Gazebo environment. Robot simulation is the key-point of modern day robotics. A suitable simulator enables the possibility to rapidly develop not only robots, but also complicated algorithms without the need for an physical prototype. Gazebo, which started of back in 2002 as plugin for ROS, offers to the user the possibility to work in an accurate model of the real-world as it includes next to state of the art physics engines and 3D graphics also several plugins to simulate real-world sensor data to work with.
	
		To work with Gazebo and custom robots it is essential to generate a model of the used robot as exact as possible to be able to transfer the results obtained in the simulation to the real robot. Also a priori knowledge about the used hardware is necessary to configure the various plugins used to simulate the robots actuator and sensors. 
		
		\subsection{Robot Description}
			
			Gazebo uses internally the SDF file format for robot description, which is a XML style markup language. More detail can be found on the SDF groups webpage\footnote{\url{http://sdformat.org/}}. If used in combination with ROS it is not recommended to write the description directly in SDF but rather in URDF (Unified Robot Description Format) which is better suited as it supports hooking ROS directly into the robots description by the use of plugins. More details on URDF can be found in the ROS wiki\footnote{\url{http://wiki.ros.org/urdf}}. Utilizing XACRO (XML Macros)\footnote{\url{http://wiki.ros.org/xacro}} allows the usage of macros inside of URDF files to further simplify those and remove not wanted code duplication.
			\newpage
			As Gazebo is capable of displaying 3D graphics it makes sense to use 3D models of the robot. These models must be available in the COLLADA (COLLAborative Design Activity)\footnote{\url{https://en.wikipedia.org/wiki/COLLADA}} format to be used directly in GAZEBO. On how to convert STEP data to COLLADA several tutorials can be found in the internet.
	
		\subsection{Plugins - Connection to ROS}
		
			As mentioned in the beginning of this section Gazebo supplies a fully fledged physics engine to simulate the behaviour of the robot in the most accurate way possible. This includes not only 'easy' simulations as gravitational forces but also more advanced calculations can be done like wheel friction or aerodynamics\footnote{\url{http://gazebosim.org/tutorials?tut=aerodynamics&cat=plugins}} Gazebo allows the user to write its own plugins to achieve the behaviour wanted.

			Based on this the integration of ROS into Gazebo is handled. Gazebo started out as plugin for ROS, now ROS is used as a plugin for Gazebo as can be seen in the following Figure \ref{fig:gazeboROS}\footnote{\url{https://bitbucket.org/osrf/gazebo_tutorials/raw/default/ros_overview/figs/775px-Gazebo_ros_api.png}}
			
			\myfigure{Gazebo_ros_api}{1.0}{fig:gazeboROS}{Gazebo and ROS}{The figure shows the interaction between Gazebo and ROS.}{h}
			\newpage
			Depending on the used ROS version, a specific Gazebo version (as suggested by the team developing Gazebo)should be used in order to avoid compatibility problems. In the project ROS Indigo LTS was used, which resulted in using Gazebo 2.X. Although as now (February 2016) the support for Gazebo 2.X was canceled, it is recommneded to move further to newer versions of Gazebo. This step was not tested, but is documented in the Gazebo wiki.
			
	\section{ROSBALL in Gazebo}
		
		The following section describes the structure of the code base used for simulating \rosball{} and explains how to use the given files to start the simulation. It is not explained how to design a new robot from scratch, as there can be various tutorials found on the internet.
		
		\subsection{Structure of code}
		
			\begin{minipage}[6cm]{0.6\textwidth}
				Figure \ref{fig:simdir} shows the folder structure used in the project which holds the necessary files for running the simulation. 
				\newline
				
				The outermost folder \textit{rosball\_simulation} is just a container, while the others - \textit{rosball\_control rosball\_description rosball\_gazebo} - are packages which can be built using catkin. 

				\begin{itemize}
					\item \textbf{rosball\_control}: Within this package the configuration - subfolder config - for the controllers used by the simulation model are stored. For the controllers existing packages were used, so no source code can be found here but only launch files, which call the according packages and nodes.
				\end{itemize}

			\end{minipage} \hfill
			\begin{minipage}{0.35\textwidth}
            	\includegraphics[width=\textwidth]{img/simulation_dir}
            	\captionof{figure}[Directory Structure Simulation-Stack]{The figure shows the directory structure of the code used for the simulation in Gazebo.}
            	\label{fig:simdir}
			\end{minipage}
			
			\begin{itemize}
				\item \textbf{rosball\_description}: As stated in the folder name this package holds the description - equal to the 3D model - of \rosball. The launch file found in this package is only for debugging purposes and is not recommended to use. 
				
				The file \textit{rosball.xacro} in the sub-folder \textit{urdf} is main file in this package. In the description extensive use of the XACRO was made, making it necessary to be familiar with XACRO before changing anything! Also the 3D model of the robots hardware - including frame, Kinect and laser scanner - can be found in this folder. 
				
				\item \textbf{rosball\_gazebo}: Within the scope of this package the configuration of Gazebo and starting the simulation is handled. All files - spread over the three mentioned folders - are connected here. So every change made to any of the files has to be validated by launching the simulation! In addition to this also all world files for Gazebo are saved within this package. These files can be created by saving the current world of Gazebo in this folder. To use the the new world name has to used to launch the simulation - see section \ref{sec:rosball_sim}
			\end{itemize}
			
			The idea to structure the files this way was taken from the Husky project\footnote{\url{}https://github.com/husky} which is available on Github. This enables to spread the files wider, making maintaining and adjusting easier but also increases complexity.		
		\subsection{How To - ROSBALL Simulation}
			\label{sec:rosball_sim}
			To launch a simulation using \rosball{} and Gazebo, based on the assumption that all necessary packages are installed (see Appendix for details), the user only has to execute only one command and specify one parameter - the world which shall be used.
			
\begin{lstlisting}[captionpos=b, language=bash, caption={Example command to launch simulation loading the gas station world.}]
roslaunch rosball_bringup simulation.launch worldName:=gas_station
\end{lstlisting}
			Using this command not only Gazebo will be started but also a joystick node will be created, that the robot can be driven around in the simulation. If no joystick is attached to the machine the node will throw an error but the rest of the command will still be executed.
			\newline
			
			The simulated robot will have, by default, a laser scanner and a Kinect attached to it. To visualize the gathered sensor information RViz must be used. Using this data the same nodes, for example map creation or autonomous navigation, can be executed. Only must be taken care of that the computer used for this task is powerful enough. The following Figure \ref{fig:gazebo} shows a screenshot of Gazebo with \rosball{} in the gas station environment.
			
			\myfigure{gazebo}{0.55}{fig:gazebo}{Gazebo and ROSBALL}{In the figure ROSBALL is shown in the simulation environment gas station. The blue plane indicates the plane of the laser scanner, also the mounted Kinect can be seen. The model shown in the image does not include the latest changes in hardware - positioning of the laser scanner. Also visible is the realistic behaviour of the laser scanner, as the objects block the rays and cast 'shadows' behind them, leaving invisible areas for the laser scanner.}{h}
						