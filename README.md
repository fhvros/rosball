# ROSBALL - README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Code base for ROSBALL robot - project done at the FHV in the course 'Applied Robotics', part of the study program 'Master Mechatronics', in the fall semester 2015
* Version 1.0

### How do I get set up? ###

The setup process is described in the included documentation. Either check the PDF file or take a look at the LATEX files.

### Contributors ###

* EBENHOCH Fabian - fabian.ebenhoch@students.fhv.at
* MARTIN Burkhard - burkhard.martin@students.fhv.at
* STÜBER Moritz - moritz.stueber@students.fhv.at